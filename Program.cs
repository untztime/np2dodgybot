using System.Diagnostics;
using TeaBot;

var clientId = "1uwqrza8uo71ouej9csrxwwplly0ii";
var host = "http://localhost:5000";

// ask for channel name
Console.WriteLine("Enter channel name:");
var channelName = Console.ReadLine();

if (string.IsNullOrEmpty(channelName))
{
    throw new Exception("Channel name cannot be empty!");
}

var url = "https://id.twitch.tv/oauth2/authorize?client_id="+clientId+"&redirect_uri="+host+"/index.html&response_type=token&scope=chat:read+chat:edit";
// thanks microsoft but EscapeDataString is not appropriate and breaks the string
#pragma warning disable SYSLIB0013
var escapedUrl = Uri.EscapeUriString(url);
#pragma warning restore SYSLIB0013

Process.Start(new ProcessStartInfo("cmd", $"/c start \"\" \"{escapedUrl}\"") { CreateNoWindow = true });
Console.WriteLine("Please authorize this app in your browser.");

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton(new Bot(channelName));

var app = builder.Build();

app.Use(async (context, next) =>
{
    var bot = app.Services.GetService<Bot>();
    if (bot is { IsBotStarted: true })
    {
        context.Response.StatusCode = 403; // Forbidden
        await context.Response.WriteAsync("The bot is already started. No further requests are allowed.");
    }
    else
    {
        await next();
    }
});


app.MapGet("/index.html", () => Results.Content(Html.GetIndexFile(host), "text/html"));
app.MapPost("/tokenEndpoint", (TwitchAccessToken token, Bot bot) =>
{
    Console.WriteLine($"Got {token.access_token} token! ");

    bot.SetAccessToken(token.access_token);
    Console.WriteLine("Starting bot...");
    bot.StartBot();
    Console.WriteLine($"Bot started!");
    
    return Results.Ok();
});



app.Run();
