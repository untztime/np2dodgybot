# NP2DodgyBot

Currently Windows only until I can be bothered to make the browser-open code multiplatform

## Getting started

1. Run NowPlaying2
3. Run this app
3. Type in your channel name
4. Authorize the twitch sign in
5. ???
6. profit hopefully


## Building
In TeaBot.csproj this is configured to make a single, self-contained .exe so user does not require .NET 7 runtime separately installed (hence why the .exe is absolutely massive)
1. Install dotnet 7.0 sdk
2. Open your favourite terminal in the folder root
3. To run: 
```ps1
dotnet run
```
4. To build:
```ps1
dotnet publish -r win-x64
```
5. Built .exes are in bin/Debug/net7.0/win-x64(or whatever)/publish/
