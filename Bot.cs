using System.Text.Json;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Clients;
using TwitchLib.Communication.Models;

namespace TeaBot;

public class Bot
{
    private TwitchClient? _client;
    private string ChannelName { get; }
    private string CurrentTrack { get; set; }
    private string CurrentArtist { get; set; }
    private string AccessToken { get; set; }
    public bool IsBotStarted { get; private set; }

    public Bot(string channelName)
    {
        ChannelName = channelName;
        CurrentTrack = "";
        CurrentArtist = "";
        AccessToken = "";
        IsBotStarted = false;
        Console.WriteLine("Bot created");
    }

    public void SetAccessToken(string accessToken)
    {
        AccessToken = accessToken;
    }

    public void StartBot()
    {
        var credentials = new ConnectionCredentials("TmTwtBot", 
            AccessToken);
        var clientOptions = new ClientOptions
        {
            MessagesAllowedInPeriod = 750,
            ThrottlingPeriod = TimeSpan.FromSeconds(30)
        };
        var customClient = new WebSocketClient(clientOptions);
        _client = new TwitchClient(customClient);
        _client.Initialize(credentials, ChannelName);

        _client.OnLog += Client_OnLog;
        _client.OnJoinedChannel += Client_OnJoinedChannel;
        _client.OnMessageReceived += Client_OnMessageReceived;
        _client.OnConnected += Client_OnConnected;
        
        _client.Connect();

        Thread thread = new(UpdateCurrentTrack);
        thread.Start();
        IsBotStarted = true;
    }
    
    private async void UpdateCurrentTrack()
    {
        var lastTrack = "";
        var lastArtist = "";
        while (_client != null)
        {
            Thread.Sleep(5000);
            try
            {
                var client = new HttpClient();
                var response = await client.GetAsync("http://localhost:9000/config");

                if (response.IsSuccessStatusCode) {
                    var json = await response.Content.ReadAsStringAsync();
                    var config = JsonSerializer.Deserialize<NowPlaying>(json);
                    if (config is { currentTrack: not null }) {
                        CurrentTrack = config.currentTrack.title;
                        CurrentArtist = config.currentTrack.artist;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (CurrentTrack == lastTrack && CurrentArtist == lastArtist) continue;
            _client.SendMessage(ChannelName, $"{CurrentArtist} - {CurrentTrack}");
            lastTrack = CurrentTrack;
            lastArtist = CurrentArtist;
        }
    }
    
    private void Client_OnLog(object? sender, OnLogArgs e)
    {
        Console.WriteLine($"{e.DateTime:o}: {e.BotUsername} - {e.Data}");
    }

    private void Client_OnConnected(object? sender, OnConnectedArgs e)
    {
        Console.WriteLine($"Connected to {e.AutoJoinChannel}");
    }

    private void Client_OnJoinedChannel(object? sender, OnJoinedChannelArgs e)
    {
        _client?.SendMessage(e.Channel, "Bot started!");
    }

    private void Client_OnMessageReceived(object? sender, OnMessageReceivedArgs e)
    {
        if (e.ChatMessage.Message.Contains("id?"))
        {
            _client?.SendMessage(e.ChatMessage.Channel,
                $"Now playing: {CurrentTrack} by {CurrentArtist}");
        }
    }
}

public class NowPlaying
{
    public CurrentTrack? currentTrack { get; set; }
}

public class CurrentTrack
{
    public string title { get; set; } = "";
    public string artist { get; set; } = "";
}