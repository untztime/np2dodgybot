namespace TeaBot;

static class Html
{
    public static string GetIndexFile(string host) 
    {
        // multiline string
        return $@"
        <!DOCTYPE html>
<html>
<head>
    <title>Redirect</title>
</head>
<body>
    <script>
        // Parse access token from URL hash
        var hash = window.location.hash.substr(1);
        var result = hash.split('&').reduce(function (res, item) {{
            var parts = item.split('=');
            res[parts[0]] = parts[1];
            return res;
        }}, {{}});

        // Pass access token to local API
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '{host}/tokenEndpoint', true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(result));
    </script>

    <h1>You can close this window now.</h1>
</body>
</html>
";
    }
}